# This class is designed to store the three scores
# for each course, each course has 3 periods
# it has the getters and setters for id, name and scores
# it also has a method to get the average of the three periods
class Score:
    def __init__(self, id, name):
        self.__id = id
        self.__name = name
        self.__scores = {1: 0, 2: 0, 3: 0}

    def get_id(self):
        return self.__id

    def get_name(self):
        return self.__name

    def get_period_score(self, period):
        return self.__scores[period]

    def get_scores(self):
        return self.__scores

    def set_id(self, id):
        self.__id = id

    def set_name(self, name):
        self.__name = name

    def set_score(self, period, score):
        self.__scores[period] = score

    def average(self):
        total = 0
        for period in self.__scores:
            total += self.__scores[period]
        return total / len(self.__scores.keys())

