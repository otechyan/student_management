from contextlib import contextmanager
from pathlib import Path
import os
import glob
import pickle
import re

EMPTY = 'This data cannot be empty.'
INVALID_RANGE = 'Invalid range'
EXTENSION = '.pic'
STUDENTS_DIRECTORY = 'students'
TEACHER_DIRECTORY = 'teachers'
COURSES_DIRECTORY = 'courses'
ADMINS_DIRECTORY = 'administrators'
OPTIONS = ['CREATE', 'DELETE', 'MODIFY', 'SEE INFO OF', 'ENROLL', 'UNENROLL', 'RETURN TO MAIN MENU']


def get_root_path():
    return '/Users/yannaigonzalez/student_management/student_management/'

def change_directory(working_directory):
    os.chdir(get_root_path() + working_directory)

def change_to_root_path():
    os.chdir(get_root_path())

# GETTING INPUT OPERATIONS
# This method validates that the user input is not empty
def get_user_input(data):
    user_input = input('Enter the {}: '.format(data))
    while user_input == '':
        print(EMPTY)
        user_input = input('Enter the {}: '.format(data))
    return user_input

# This method gets any user input and returns a valid integer number
def get_valid_integer(data, start=0, end=2020):
    user_input = 0
    while user_input == 0:
        try:
            user_input = int(get_user_input(data))
            birth = int(user_input)
            if not (birth >= start and birth <= end):
                print(INVALID_RANGE)
                print('The range must be between {} and {}'.format(start, end))
                user_input = 0
        except:
            print('The data you entered is not a valid number')
            user_input = 0
    return user_input

def confirm_operation(message):
    print(f'Do you want to confirm {message}? ', end='')
    confirmation = get_user_input('confirmation (y for yes other key for no)')
    return confirmation.upper()

# This method change to the given directory, yields and return to
# original directory
@contextmanager
def method_in_directory(object_directory):
    original_directory = os.getcwd()
    try:
        change_directory(object_directory)
        yield
    finally:
        os.chdir(original_directory)

# This method uses method_in_directory to capture a method and execute it
def perform_operation(object_directory, method, func_args=[], func_kwargs={}):
    with method_in_directory(object_directory):
        return method(*func_args, **func_kwargs)

# This method change to a directory

# SAVING OPERATIONS
# This method saves an object object into a pickle file
def save_object(id, object, object_type):
    file = open(str(id) + EXTENSION, 'wb')
    pickle.dump(object, file)
    file.close()
    print(f'You have saved {object_type} with id {id}.')

def confirm_changes(id, object, object_type):
    confirmation = confirm_operation('saving changes')
    if confirmation == 'Y':
        save_object(id, object, object_type)
        print('Changes have been made!')
    else:
        print('No changes have been made!')


# CREATING OPERATIONS
# This method creates and id is based in the current elements of the directory
def create_id():
    files = glob.glob('*' + EXTENSION)
    if files:
        id = max(retrieve_all_ids()) + 1
    else:
        id = 1
    return id

# DELETE OPERATIONS FOR OBJECTS
# This method delete a file associated with an object
def delete_object(id, object_type):
    confirmation = confirm_operation(f'delete {object_type}')
    if confirmation == 'Y':
        os.remove(str(id) + '.pic')
        print(f'{object_type} with id {id} has been deleted.')

# This method retrieve all the objects objects from the current directory
def retrieve_all_objects():
    objects = []
    for file in glob.glob('*' + EXTENSION):
        file = open(file, 'rb')
        object = pickle.load(file)
        file.close()
        objects.append(object)
    return objects

# This method validate if there is a file associated with an id
def id_exists(id):
    exists = os.path.exists(str(id) + EXTENSION)
    return exists

# This method returns the object id, if the object id does not exist returns 0
def get_valid_id(object_type):
    id = get_valid_integer(f'{object_type} id')
    if id_exists(id):
        object = retrieve_object(id)
        object_id = object.get_id()
    else:
        print(f'The {object_type} with id {id} does not exist.')
        object_id = 0
    return object_id

# This method gets the object from a file given the id
def retrieve_object(id):
    file = open(str(id) + EXTENSION, 'rb')
    member = pickle.load(file)
    file.close()
    return member

# This method retrieve all the id numbers
def retrieve_all_ids():
    all_ids = []
    for file_name in glob.glob('*' + EXTENSION):
        id = re.search(r'(\d+)', file_name).group(1)
        all_ids.append(int(id))
    all_ids.sort()
    return all_ids

# This method creates the appropiate menu
# that teacher, administrator, student and course have in common
def build_menu(directory):
    member = directory[:-1].upper()
    menu = ''
    option_number = 1
    for option in OPTIONS[:6]:
        menu += f'{option_number}: {option} {member}\n'
        option_number += 1
    #additional, option_number = additional_options(directory, member, option_number) 
    #menu += additional
    menu += f'{option_number}: {OPTIONS[-1]}\n'
    return menu, option_number


# This method shows all the options available and prints
# depending if the member is teacher, administrator or student
def additional_options(directory, member, option_number):
    additional = ''
    if directory == STUDENTS_DIRECTORY:
        for option in OPTIONS[4:-1]:
            additional += f'{option_number}: {option}\n'
            option_number += 1
    return additional, option_number

# This method shows the menu based in the directory and the group of data of
# each object
def show_menu_modify(member_type, data_group):
    menu_counter = 0
    for data in data_group:
        menu_counter += 1
        print(f'{menu_counter}: Modify {data} of the {member_type}.')

    menu_counter += 1
    print('{}: Return to previous menu.'.format(menu_counter))

