import school_member
import score

# Administrator class inherits all the properties and methods from SchoolMember class
class Administrator(school_member.SchoolMember):
    def __init__(self, first_name, last_name, year, month, day, id):
        school_member.SchoolMember.__init__(self, first_name, last_name, year, month, day, id)
