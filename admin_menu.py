import os
import member_operations as memo
import general_operations as geno
import student_operations as stuo
import course_operations as coop
import teacher_operations as teach
from student import Student
from teacher import Teacher
from course import Course

OPTIONS = {1: 'students', 2: 'teachers',  3: 'courses', 4: 'quit'}
CLASSES = {'students': Student, 'teachers': Teacher, 'courses': Course}

def show_main_menu():
    print('Select an option')
    options_menu = ''
    for option in OPTIONS:
        if not option == 4: options_menu += f'{option}: {OPTIONS[option].upper()} operations\n'
        else:
            options_menu += f'{option}: {OPTIONS[option].upper()}\n'
    print(options_menu)


# This method is the main point for member operations
def menu_for_member(member_directory):
    menu, options_length = geno.build_menu(member_directory)
    option = 0
    while option != options_length:
        print(menu)
        option = int(geno.get_valid_integer('option', 1, options_length))
        if option != options_length:
            choice_for_member(option, member_directory)

def choice_for_member(option, member_directory):
    member_type = member_directory[:-1]
    if option == 1:
        memo.create_member(CLASSES[member_directory], member_directory)
    else:
        member_id = geno.get_valid_id(member_type)
        if member_id:
            if option == 2:
                coop.remove_all_courses_from_member(member_id, member_directory)
                memo.delete_member(member_id, member_type)
            elif option == 3:
                memo.modify_member(member_id, member_type)
            elif member_directory == geno.STUDENTS_DIRECTORY:
                menu_for_student(member_id, option)
            elif member_directory == geno.TEACHER_DIRECTORY:
                menu_for_teacher(member_id, option)

# This method show the specific options for student
def menu_for_student(student_id, option):
    if option == 4:
        student = geno.retrieve_object(student_id)
        stuo.display_student_info(student)
    elif option == 5 or option == 6:
        course_ids = geno.retrieve_all_ids()
        course_id = geno.perform_operation(geno.COURSES_DIRECTORY, geno.get_valid_id, ['course'])
        if course_id:
            if option == 5:
                geno.perform_operation(geno.COURSES_DIRECTORY, coop.enroll, [student_id, course_id, geno.STUDENTS_DIRECTORY])
            elif option == 6:
                geno.perform_operation(geno.COURSES_DIRECTORY, coop.remove_member_from_course, [student_id, course_id, geno.STUDENTS_DIRECTORY])

# This method shows the specific options for teacher
def menu_for_teacher(teacher_id, option):
    if option == 4:
        teach.display_teacher_information(teacher_id)
    elif option == 5 or option == 6:
        course_id = geno.perform_operation(geno.COURSES_DIRECTORY, geno.get_valid_id, ['course'])
        if course_id:
            if option == 5:
                geno.perform_operation(geno.COURSES_DIRECTORY, coop.enroll, [teacher_id, course_id, geno.TEACHER_DIRECTORY])
            elif option == 6:
                geno.perform_operation(geno.COURSES_DIRECTORY, coop.remove_member_from_course, [teacher_id, course_id, geno.TEACHER_DIRECTORY])

# This method displays the menu for courses
def menu_for_courses():
    option = 0
    while option != 5:
        print('1: Create a course')
        print('2: Delete a course')
        print('3: Modify a course')
        print('4: See course info')
        print('5: Return to previous menu')
        option = int(geno.get_valid_integer('option', 1, 5))
        if option == 1:
            course_id = geno.create_id()
            coop.create_course(course_id)
        elif option < 5:
            course_id = geno.get_valid_id(geno.COURSES_DIRECTORY[:-1])
            if course_id:
                if option == 2:
                    print(os.getcwd())
                    coop.delete_course(course_id)
                elif option == 3:
                    coop.modify_course(course_id)
                elif option == 4:
                    coop.display_course_info(course_id)

# This is the main method for the admin menu
def main(admin_id):
    option = 0
    while option != 4:
        show_main_menu()
        option = int(geno.get_valid_integer('option', 1, 4))

        if option == 1 or option == 2:
            geno.perform_operation(OPTIONS[option], menu_for_member, [OPTIONS[option]])
        elif option == 3:
            geno.perform_operation(OPTIONS[option], menu_for_courses)

main(1)
