import course
import student
import general_operations as geno
import teacher_operations as teach


STUDENTS_DIRECTORY = 'students'
COURSE_INFO = ['name']
COURSE_DIRECTORY = 'courses'

# This method retrieves the basic info for a course
# wich consists of the values given in COURSE_INFO
def get_course_info():
    course_info = {}
    for info in COURSE_INFO:
        course_info[info] = geno.get_user_input(info)
    return course_info

# This method get info from the user to create a new Course object
# and save it to its file in directory 'courses'
def create_course(id):
    course_info = get_course_info()
    course_object = course.Course(id, course_info['name'])
    geno.confirm_changes(id, course_object, 'course')

# This method delete a course a remove its id from the student information
def delete_course(course_id):
    course = geno.retrieve_object(course_id)
    geno.perform_operation(geno.STUDENTS_DIRECTORY, unenroll_all_students, [course])
    if course.teacher_in_course():
        geno.perform_operation(geno.TEACHER_DIRECTORY, remove_teacher, [course])
    print('All students have been unenrolled from course.')
    geno.delete_object(course_id, 'course')

# This method remove the id from all the students scores
def unenroll_all_students(course):
    for student_id in course.get_students():
        st = geno.perform_operation(geno.STUDENTS_DIRECTORY, retrieve_object, [student_id])
        st.remove_score(course.get_id())
        geno.perform_operation(geno.STUDENTS_DIRECTORY, geno.save_object, [st, 'student'])

# This method remove the id of the course associated with the teacher
def remove_teacher(course):
    teacher = geno.perform_operation(geno.TEACHER_DIRECTORY, retrieve_object, [student_id])
    teacher.remove_course(course.get_id())
    geno.perform_operation(geno.TEACHER_DIRECTORY, geno.save_object, [teacher, 'teacher'])

# This method shows the menu for modify a course
def modify_object_course(course):
    geno.show_menu_modify('course', COURSE_INFO)
    option = int(geno.get_valid_integer('option', 1, 2))
    while option != 2:
        if option == 1:
            info = geno.get_user_input('new {}'.format(COURSE_INFO[option - 1]))
            if option == 1:
                course.set_name(info)
        geno.show_menu_modify('course', COURSE_INFO)
        option = int(geno.get_valid_integer('option', 1, 2))
    return course

# This method modify the course
def modify_course(id):
    course = geno.retrieve_object(id)
    course = modify_object_course(course)
    geno.confirm_changes(id, course, 'course')

# This method add a student to a course
def enroll(member_id, course_id, member_directory):
    member_type = member_directory[:-1]
    course = geno.retrieve_object(course_id)
    member = geno.perform_operation(member_directory, geno.retrieve_object, [member_id])
    if course.member_in_course(member_type, member_id):
        print(f'This {member_type} is already enrolled in {course.get_name()}')
    else:
        if member_directory == geno.STUDENTS_DIRECTORY:
            course.add_student(member_id)
            member.add_score(course_id, course.get_name())
        else:
            course.add_teacher(member_id)
            member.add_course(course_id)

        print('Do you want to save the changes? ', end='')
        confirmation = geno.get_user_input('confirmation (y for yes other key for no)')
        if confirmation.upper() == 'Y':
            geno.save_object(course_id, course, 'course')
            geno.perform_operation(member_directory, geno.save_object, [member_id, member, member_type])
            print('Changes have been made!')
        else:
            print('No changes have been made!')

# This method remove a student from a course
def remove_member_from_course(member_id, course_id, member_directory):
    print('inside member from course')
    course = geno.retrieve_object(course_id)
    try:
        if member_directory == geno.STUDENTS_DIRECTORY:
            course.remove_student(member_id)
        else:
            course.remove_teacher(member_id)

        member = geno.perform_operation(member_directory, geno.retrieve_object, [member_id])
        confirmation = geno.get_user_input('confirmation (y for yes other key for no)')
        if confirmation.upper() == 'Y':
            geno.save_object(course_id, course, 'course')

            if member_directory == geno.STUDENTS_DIRECTORY:
                member.remove_score(course_id)
                geno.perform_operation(geno.STUDENTS_DIRECTORY, geno.save_object, [member_id, member, 'student'])
            else:
                member.remove_course(course_id)
                geno.perform_operation(geno.TEACHER_DIRECTORY, geno.save_object, [member_id, member, 'teacher'])
            print('Changes have been made!')
        else:
            print('No changes have been made!')
    except:
        print(f'This {member_directory[:-1]} does not belong to this course.')

# This method removes all the courses from a member
def remove_all_courses_from_member(member_id, member_directory):
    member = geno.retrieve_object(member_id)

    if member_directory == geno.STUDENTS_DIRECTORY:
        for score_id in member.get_scores():
            geno.perform_operation(geno.COURSES_DIRECTORY, remove_member_from_course, [member_id, score_id, geno.STUDENTS_DIRECTORY])
    else:
        for course_id in member.get_courses():
            geno.perform_operation(geno.COURSES_DIRECTORY, remove_member_from_course, [member_id, course_id, geno.TEACHER_DIRECTORY])

def get_students_in_a_course(course_id):
    course = geno.retrieve_object(course_id)
    students_id = course.get_students()
    students = []
    for student_id in students_id:
        student = geno.perform_operation(geno.STUDENTS_DIRECTORY, geno.retrieve_object, [student_id])
        students.append(student)
    return students

def display_course_info(course_id):
    course = geno.retrieve_object(course_id)
    teacher_id = course.get_teacher()
    print(f'Course Name: {course.get_name()}')
    print('Teacher')
    if teacher_id:
        teacher = geno.perform_operation(geno.TEACHER_DIRECTORY, geno.retrieve_object, [teacher_id])
        print(f'{teacher.get_full_name()}')
    else:
        print('There is not teacher assigned to this course yet.')
    print('Students')
    teach.display_students_scores(course_id, 1, True)

#display_course_info(1)
def display_all_courses():
    courses = geno.retrieve_all_objects()
    print('Id    Name')
    for course in courses:
        print(f'{course.get_id()}    {course.get_name()}')
