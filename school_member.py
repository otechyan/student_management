# This class is the parent class for Student, Teacher and Administrator Class
# it contains all the data state and getters and setters methods that the teacher, student and administrator will use

class SchoolMember:
    def __init__(self, first_name, last_name, year, month, day, id):
        self.__first_name = first_name
        self.__last_name = last_name
        self.__year_of_birth = year
        self.__month_of_birth = month
        self.__day_of_birth = day
        self.__username = (first_name[0] + last_name + str(year)).lower()          # The username takes the first letter of the firstname all the last name and year of birth
        self.__password = str(month) + str(day) + first_name.capitalize()      # The password uses the combination of the month, the day and the firstname capitalized 
        self.__id = id

# Setter methods

    def set_first_name(self, first_name):
        self.__first_name = first_name

    def set_last_name(self, last_name):
        self.__last_name = last_name

    def set_year_of_birth(self, year):
        self.__year_of_birth = year

    def set_month_of_birth(self, month):
        self.__month_of_birth = month

    def set_day_of_birth(self, day):
        self.__day_of_birth = day

    def set_username(self, username):
        self.__username = username

    def set_password(self, password):
        self.__password = password

    def set_id(self, id):
        self.__id = id

# Getter methods

    def get_first_name(self):
        return self.__first_name

    def get_last_name(self):
        return self.__last_name

    def get_year_of_birth(self):
        return self.__year_of_birth

    def get_month_of_birth(self):
        return self.__month_of_birth

    def get_day_of_birth(self):
        return self.__day_of_birth

    def get_username(self):
        return self.__username

    def get_password(self):
        return self.__password

    def get_id(self):
        return self.__id

    def get_full_name(self):
        return self.get_first_name() + ' ' + self.get_last_name()

# Print member info
    def __str__(self):
        member_info = (f'ID: {self.__id}\nName: {self.__first_name} {self.__last_name}\n'
            f'Date of birth: {self.__month_of_birth}/{self.__day_of_birth}/{self.__year_of_birth}\n'
            f'username: {self.__username}\n')
        return member_info

