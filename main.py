import sys
import administrator
import general_operations as geno
import member_operations as memo
import admin_menu


ADMINS_DIRECTORY = 'administrators'
PROFILES = {1: geno.ADMINS_DIRECTORY, 2: geno.TEACHER_DIRECTORY, 3: geno.STUDENTS_DIRECTORY, 4: 'quit'}

# This method runs the first time the administrator runs the program
# it helps to create the main administrator
def setup():
    print('WELCOME TO THE STUDENT MANAGEMENT PROGRAM')
    print('First you need to create your user administrator')
    memo.create_member(PROFILES[1], administrator.Administrator)
    admin = geno.retrieve_member(1, PROFILES[1])
    print(f'Your username is: {admin.get_username()}')
    print(f'Your password is: {admin.get_password()}')
    return admin.get_id()

# This method shows the main options once the first administrator was created.
def main_menu():
    while True:
        for option in PROFILES:
            print(f'{option}: {PROFILES[option].upper()}')

        option = geno.get_valid_integer('profile, (4 to exit)', 1, 4)
        if option == 4:
            sys.exit()

        user_id = memo.authenticate(PROFILES[option])
        if user_id:
            if option == 1:
                admin_menu.main(user_id)
            elif option == 2:
                student_menu.main(user_id)
            elif option == 3:
                teacher_menu.main(user_id)
        else:
            print(f'The user and/or password for this {PROFILES[option][:-1]} is incorrect.')


def main():
    try:
        geno.retrieve_member(1, PROFILES[1])
        admin_id = 0
    except:
        admin_id = setup()

    if admin_id:
        admin_menu.main(admin_id)
    else:
        main_menu()

main()

