import school_member
import score

SCORE_1 = 'score 1'
SCORE_2 = 'score 2'
SCORE_3 = 'score 3'
COURSE_NAME = 'course name'
COURSE_ID = 'id'

# Student class inherits all the properties and methods from SchoolMember class
class Student(school_member.SchoolMember):
    def __init__(self, first_name, last_name, year, month, day, id):
        school_member.SchoolMember.__init__(self, first_name, last_name, year, month, day, id)
        self.__scores = {}

    def get_scores(self):
        return self.__scores

    # This method returns the scores based on the id of the Course class
    def get_score_by_id(self, score_id):
        return self.__scores[score_id]

    def get_score_info(self, score_id):
        score_info = {}
        score = self.get_score_by_id(score_id)
        score_info[SCORE_1] = score.get_period_score(1)
        score_info[SCORE_2] = score.get_period_score(2)
        score_info[SCORE_3] = score.get_period_score(3)
        score_info[COURSE_NAME] = score.get_name()
        score_info[COURSE_ID] = score.get_id()
        return score_info

    def set_score(self, score_id, period, student_score):
        score = self.__scores[score_id]
        score.set_score(period, student_score)

    def add_score(self, score_id, name):
        new_score = score.Score(score_id, name)
        self.__scores[score_id] = new_score

    def remove_score(self, score_id):
        self.__scores.pop(score_id)

    def get_total_scores(self):
        return len(self.__scores)

    def get_total_by_period(self, period):
        total = 0
        for course_id in self.get_scores():
            sc = self.get_score_by_id(course_id)
            total += sc.get_period_score(period)
        return total

    # This method returns the average of all the courses the Student is enrolled in
    def get_average_by_period(self, period):
        return self.get_total_by_period(period) / self.get_total_scores()

