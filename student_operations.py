import student as st
import member_operations as memo
import general_operations as geno
import course_operations as coop

COURSE_NOT_FOUND = 'Course Not Found'

# These method deletes a students and the reference in the courses it belonged
def delete_student(student_id):
    student = geno.retrieve_member()
    for course_id in student.get_scores():
        geno.perform_operation(geno.COURSES_DIRECTORY, coop.remove_student_from_course, [student_id, course_id, 'students'])
    memo.delete_member(student_id)


# This method display scores for every course the student is enrolled in
def display_scores(scores_info, course_name=True):
    for score in scores_info:
        if course_name:
            print(f"Course name: {score['course name']}")
        print(f"First score: {score['score 1']}")
        print(f"Second score: {score['score 2']}")
        print(f"Third score: {score['score 3']}")


# This method get the scores for a course
def get_score_info(student, course_id):
    try:
        score_info = student.get_score_info(course_id)
    except:
        score_info = COURSE_NOT_FOUND
    return score_info

# This method get all the info of student's scores
def get_scores_info(student):
    scores_info = []
    score_ids = list(student.get_scores().keys())
    score_ids.sort()
    for id in score_ids:
        scores_info.append(get_score_info(student, id))
    return scores_info

# This method display the scores of one course
def display_course_score(student, course_id, course_name=True):
    score_info = get_score_info(student, course_id)

    if score_info != COURSE_NOT_FOUND:
        display_scores([score_info], course_name)
    else:
        print(COURSE_NOT_FOUND)

# This method list average of all courses for a period
def display_average_of_a_period(student, period):
    print(f'The total score for period {period} is {student.get_total_by_period(period)}')
    print(f'The average for {student.get_total_scores()} courses is {student.get_average_by_period(period)}')

# This method show the student info, except the password
def display_student_info(student, course_name=True):
    print(student)
    scores_info = get_scores_info(student)
    if scores_info:
        display_scores(scores_info)
    else:
        print('There are no courses available for you.')


# This method list the courses the student is assigned to and the name of the teacher


