import os
import general_operations as geno
from student import Student
from teacher import Teacher

DATA_NAME = ['first name', 'last name']
DATA_BIRTH = {'year': {'start': 1920, 'end': 2021}, 'month': {'start': 1,'end': 12}, 'day': {'start':1, 'end':31}}
DATA_BASIC = DATA_NAME + ['username', 'password'] + list(DATA_BIRTH.keys())

# This method gets the new user data, it returns the first name, last name, year, month and day of birth
def get_school_member_data():
    member_data = {}
    for data in DATA_NAME:
        member_data[data] = geno.get_user_input(data)

    for data in DATA_BIRTH:
        member_data[data] = geno.get_valid_integer(data + ' of birth', DATA_BIRTH[data]['start'], DATA_BIRTH[data]['end'])

    return member_data

# This method create a new member
# it stores the member data in a pickle object stored
# named students, teachers or administrators
def create_member(class_type, member_directory):
    member_data = get_school_member_data()
    id = geno.create_id()
    new_member = class_type(member_data['first name'], member_data['last name'], member_data['year'],
                              member_data['month'], member_data['day'], id)
    geno.save_object(id, new_member, member_directory[:-1])

# This method delete a member
def delete_member(id, member_type):
    geno.delete_object(id, member_type)

# This method gets a member object and return the same member object
# with or without modifications
def modify_object_member(member, member_type):
    geno.show_menu_modify(member_type, DATA_BASIC)
    option = int(geno.get_valid_integer('option', 1, 8))
    while option != 8:
        if option == 1 or option == 2 or option == 3 or option == 4:
            info = geno.get_user_input('new {}'.format(DATA_BASIC[option - 1]))
            if option == 1:
                member.set_first_name(info)
            elif option == 2:
                member.set_last_name(info)
            elif option == 3:
                member.set_username(info)
            else:
                member.set_password(info)
        elif option == 5 or option == 6 or option == 7:
            start = DATA_BIRTH[DATA_BASIC[option - 1]]['start']
            end = DATA_BIRTH[DATA_BASIC[option - 1]]['end']
            info = geno.get_valid_integer('new {}'.format(DATA_BASIC[option - 1]), start, end)
            if option == 5:
                member.set_year_of_birth(info)
            elif option == 6:
                member.set_month_of_birth(info)
            else:
                member.set_day_of_birth(info)
        geno.show_menu_modify(member_type, DATA_BASIC)
        option = int(geno.get_valid_integer('option', 1, 8))
    return member

# and modify its content
def modify_member(id, member_type):
    member = geno.retrieve_object(id)
    member = modify_object_member(member, member_type)
    geno.confirm_changes(id, member, member_type)

# This method displays basic data of the school member
# first name, last name, year, month and day of birth,
# and username
def show_basic_member_info(member):
    print('Basic information:')
    print('Full Name: {} {}'.format(member.get_first_name(), member.get_last_name()))
    print('Date of birth: {}/{}/{}'.format(member.get_month_of_birth(), member.get_day_of_birth(), member.get_year_of_birth()))
    print('Username: {}'.format(member.get_username()))

# This method returns 0 if the username and password are not found
# if the user and password are found it returns the user id
def authenticate():
    user_id = 0
    username = geno.get_user_input('user')
    password = geno.get_user_input('password')
    members = geno.retrieve_all_objects()
    for member in members:
        if member.get_username() == username and member.get_password() == password:
            user_id = member.get_id()
    return user_id
