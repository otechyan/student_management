import course
import general_operations as geno
import student_operations as stuo
import course_operations as coop
import member_operations as memo

# This method assigns a course to a teacher
def assign_course(teacher_id, course_id):
    teacher = geno.retrieve_object(teacher_id)
    if not teacher.teacher_in_course(course_id):
        teacher.add_course(course_id)
        geno.confirm_changes(teacher_id, teacher, 'teacher')
    else:
        print(f'The teacher with id {teacher_id} is already assigned to course with id {course_id}')

# This method removes a course from a teacher
def remove_course(teacher_id, course_id):
    teacher = geno.retrieve_object(teacher_id)
    if teacher.teacher_in_course(course_id):
        teacher.remove_course(course_id)
        geno.confirm_changes(teacher_id, teacher, 'teacher')
    else:
        print(f'The teacher with id {teacher_id} is not assigned to course with id {course_id}')

# This method put a score to a student
def assign_score(student_id, course_id, period, score):
    student = geno.retrieve_object(student_id)
    student.set_score(course_id, period, score)
    geno.confirm_changes(student_id, student)

# This method list students scores in the course and their scores
def display_students_scores(course_id, student_id, all_students=False):
    if all_students:
        students = coop.get_students_in_a_course(course_id)
    else:
        students = [geno.retrieve_object(student_id)]

    if students:
        for st in students:
            print(f'Student ID: {st.get_id()}')
            print(f'Name: {st.get_full_name()}')
            stuo.display_course_score(st, course_id, course_name=False)
    else:
        print('There are no students assigned yet')

# List teacher information
def display_teacher_information(teacher_id):
    teacher = geno.retrieve_object(teacher_id)
    print(teacher)
    print('Courses asigned:')
    if teacher.courses_exist():
        for course_id in teacher.get_courses():
            course = geno.perform_operation(geno.COURSES_DIRECTORY, geno.retrieve_object, [course_id])
            print(course.get_name())
    else:
        print('There are no courses assigned to you yet.')
