import school_member
import score

# Teacher class inherits all the properties and methods from SchoolMember class
class Teacher(school_member.SchoolMember):
    def __init__(self, first_name, last_name, year, month, day, id):
        school_member.SchoolMember.__init__(self, first_name, last_name, year, month, day, id)
        self.__courses = []

    def get_courses(self):
        return self.__courses

    def add_course(self, course_id):
        self.__courses.append(course_id)

    def remove_course(self, course_id):
        self.__courses.remove(course_id)

    def teacher_in_course(self, course_id):
        return course_id in self.get_courses()

    def courses_exist(self):
        return len(self.__courses) > 0
