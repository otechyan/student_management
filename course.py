class Course:
    def __init__(self, id, name):
        self.__id = id
        self.__name = name
        self.__students = []
        self.__teacher_id = 0

    def set_id(self, id):
        self.__id = id

    def set_name(self, name):
        self.__name = name

    def set_students(self, students_id):
        self.__students = students_id

    def get_id(self):
        return self.__id

    def get_name(self):
        return self.__name

    def get_students(self):
        return self.__students

    def get_teacher(self):
        return self.__teacher_id

    def add_student(self, student_id):
        self.__students.append(student_id)

    def add_teacher(self, teacher_id):
        self.__teacher_id = teacher_id

    def remove_student(self, student_id):
         self.__students.remove(student_id)

    def remove_teacher(self, teacher_id):
        self.__teacher_id = 0

    def student_in_course(self, student_id):
        return student_id in self.__students

    def teacher_in_course(self, teacher_id=0):
        return self.__teacher_id > 0

    def member_in_course(self, member_type, member_id):
        if member_type == 'student':
            return self.student_in_course(member_id)
        else:
            return self.teacher_in_course(member_id)
